class SessionsController < ApplicationController
  def new
  end

  def signup
    @user = User.new
  end

  def create
    user = User.find_by(username: params[:session][:username])
    if user && user.password==(params[:session][:password])
      #session[:user_id] = user.id
      #SessionsHelper.log_in user
      log_in user
      redirect_to home_url
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
  end
end
