class UsersController < ApplicationController

  def create
    @user = User.new(user_params)
    if @user.save
      #flash[:success] = "Welcome to the Sample App!"
      redirect_to auth_welcome_url
    else
      render 'sessions/welcome'
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :password)

  end



end
